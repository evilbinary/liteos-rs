extern crate alloc;

use self::alloc::vec::Vec;
use core::alloc::Layout;
use cortex_m::asm;
use alloc_cortex_m::CortexMHeap;



#[inline]
pub fn heap_start() -> *mut u32 {
    extern "C" {
        static mut __sheap: u32;
    }

    unsafe { &mut __sheap }
}

pub fn init(heap_size:usize) {
	unsafe { ALLOCATOR.init(heap_start() as usize, heap_size) }
}

pub fn alloc(size:usize) -> OMem {
	let mut mem = Mem {vec:Vec::new()};
	if size > 0 {
		mem.vec.resize(size, 0);
	}
	Some(mem)
}

pub fn realloc(mem:&mut Mem, size:usize) {
	mem.vec.resize(size, 0);
}

pub type OMem = Option<Mem>;

#[derive(Clone)]
pub struct Mem {
	vec:Vec<u8>,
}

impl Mem {
	pub fn write(&mut self, a:&[u8]) {
		for i in 0..a.len() {
			if i >= self.vec.len() {
				panic!("mm out")
			}
			self.vec[i] = a[i];
		}
	}
	pub fn read(&self, offset:usize) -> u8 {
		self.vec[offset]
	}
}

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

#[alloc_error_handler]
fn alloc_error(_layout: Layout) -> ! {
    asm::bkpt();

    loop {}
}
