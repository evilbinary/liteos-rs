ENTRY(_start)
K_VIR_ADDR = 0x00000;
K_PHY_ADDR = 0x00000;
PAGE_SIZE    = 0x100;
SECTIONS
{
        /* Code. */
        .text K_VIR_ADDR : AT(K_PHY_ADDR)
        {
		. = ALIGN(PAGE_SIZE);
                *(.text._start)
        }
        
        /* Read-only data. */
        .rodata  : ALIGN(4K)
        {
                *(.rodata)
        }

        /* Read-write data (initialized) */
        .data  : ALIGN(4K)
        {
                *(.data )
                _stack_top = . ; 
        }
        
        /* Read-write data (uninitialized) and stack */
        .bss  : ALIGN(4K)
        {
                *(COMMON)
                *(.bss)
        }
        .ARM.exidx :{
                *(.ARM)
        }
       _end = .;
}
